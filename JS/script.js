for(var i = 10; i <= 20; i++){
    console.log(i);
}
console.log("-----------------------------")
for(var i = 10; i <= 20; i++){
    console.log(i*i);
}
console.log("-----------------------------")
var i = 10;
var sum = 0;
for( i ; i <= 20; i++){
    sum += i;
}
console.log(sum);
console.log("-----------------------------")

function localAlert(message){
    var alertBox = document.getElementById('exeptions');
    alertBox.innerHTML = message;
}

function checkInput() {

    localAlert("");

    var firstVal = document.getElementById('x1').value;
    var secondVal = document.getElementById('x2').value;

    if(!(firstVal || secondVal)){

        localAlert("Enter values");
        return;

    } else if (!firstVal) {

        localAlert("Enter the first value");
        return;

    } else if (!secondVal) {

        localAlert("Enter the second value");
        return;
    }

    var x1 = parseInt(document.getElementById('x1').value);
    var x2 = parseInt(document.getElementById('x2').value);

    if(!(x1 || x2)){

        localAlert("Inputs are not numbers");
        return;

    } else if(!x1) {

        localAlert("First inputs is not a number");
        return;

    } else if (!x2) {

        localAlert("Second inputs is not a number");
        return;
    }
}

function getSum() {

    checkInput();

    var x1 = parseInt(document.getElementById('x1').value);
    var x2 = parseInt(document.getElementById('x2').value);

    var resultDiv = document.getElementById('result');

    if(document.getElementById('numSum').checked)
        resultDiv.innerHTML = ("x1 + x2 = " + (x1+x2));
    else 
        resultDiv.innerHTML = ("x1 * x2 = " + (x1*x1));
}

function getClear() {

    document.getElementById('x1').value = "";
    document.getElementById('x2').value = "";
}

function getSimple() {

    checkInput();

    var resultDiv = document.getElementById('result');
    resultDiv.innerHTML = "";

    var x1 = parseInt(document.getElementById('x1').value);
    var x2 = parseInt(document.getElementById('x2').value);

    if(x1 > x2){
        var x3 = x1;
        x1 = x2;
        x2 = x3;
    }

    for(var j = x1; j <= x2; j++){

        var i = 2;

        for(i; i <= j; i++){

            if(j%i == 0) 
                break;
        }

        if(i==j)
            resultDiv.innerHTML += j + " ";
    }
}

